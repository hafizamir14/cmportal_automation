<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_CoHortSave</name>
   <tag></tag>
   <elementGuidId>773d2c0b-5cfe-420b-9662-2962116b0466</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button[class='confirm btn btn-primary']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ref_element = 'Object Repository/OR_Telehealth/Obj_frame']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//div[@id='tocGrid']//table)[2]//tr[1]//td[57]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_Telehealth/Obj_frame</value>
   </webElementProperties>
</WebElementEntity>
