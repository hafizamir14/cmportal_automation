<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_SuperBill_Diagnosis_SaveClose</name>
   <tag></tag>
   <elementGuidId>b5c9be9d-ecfc-4451-8376-a7ea3a1a9b4f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button[id='encounterSoapBtnSaveNClose']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ref_element = 'Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_Close Popup/Obj_CCMFrame']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//div[@id='tab_content_superbill']//div)[55]//button[contains(text(), 'Apply')]</value>
      <webElementGuid>503f4490-0712-421f-bd78-d27f749b116a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_Close Popup/Obj_CCMFrame</value>
      <webElementGuid>63268542-3712-4bbb-80c1-1f46758eaec2</webElementGuid>
   </webElementProperties>
</WebElementEntity>
