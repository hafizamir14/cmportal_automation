<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_InsuranceDeleteConfirm</name>
   <tag></tag>
   <elementGuidId>e8721562-dd09-41b0-a13d-95779a0c1fe0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@ref_element = 'Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_Close Popup/Obj_CCMFrame']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@class='confirm btn btn-primary']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@placeholder='Search and select analyte']</value>
      <webElementGuid>4640a305-6207-4cc6-abb8-2196275b8d17</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_Close Popup/Obj_CCMFrame</value>
      <webElementGuid>be3faaa5-bde2-4ea6-8655-0db2d0a1d1c5</webElementGuid>
   </webElementProperties>
</WebElementEntity>
