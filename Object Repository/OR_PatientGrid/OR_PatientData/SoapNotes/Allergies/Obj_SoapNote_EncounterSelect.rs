<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_SoapNote_EncounterSelect</name>
   <tag></tag>
   <elementGuidId>c804b367-2183-49d6-9bab-7e45c2412b4a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//ul[@id=&quot;encounterSoapCode_listbox&quot;]//li[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ref_element = 'Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_Close Popup/Obj_CCMFrame']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class='clinical problem k-grid has-wizard mz-widget k-widget k-editable editable']/div[1]/div/button[1]</value>
      <webElementGuid>daae11f2-ea55-42f7-9c69-8331bda8db2f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_Close Popup/Obj_CCMFrame</value>
      <webElementGuid>b6105d2a-5a9b-4080-bea0-03778f30b336</webElementGuid>
   </webElementProperties>
</WebElementEntity>
