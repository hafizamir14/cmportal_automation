<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_Entered_ProcedureTargetSite</name>
   <tag></tag>
   <elementGuidId>a5051521-1ea4-4bfb-b750-79d8ba1ed2c9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//div[@data-mz-key='clinical.procedure'])/div[3]//tr[1]//td[6]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ref_element = 'Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_Close Popup/Obj_CCMFrame']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class='clinical lab k-grid has-wizard list-mode-only mz-widget k-widget k-editable batch-editing editable']/div[3]/table/tbody/tr[2]/td[2]</value>
      <webElementGuid>c52427ff-dbf7-4ddf-a8a6-3fe9a5dbf477</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_Close Popup/Obj_CCMFrame</value>
      <webElementGuid>5cde120f-f76a-4e34-97be-ba8a6c5b3bc8</webElementGuid>
   </webElementProperties>
</WebElementEntity>
