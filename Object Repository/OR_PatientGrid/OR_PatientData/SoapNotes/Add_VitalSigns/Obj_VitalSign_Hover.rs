<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_VitalSign_Hover</name>
   <tag></tag>
   <elementGuidId>cf19268e-ee88-4609-929f-3296f3a78624</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@ref_element = 'Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_Close Popup/Obj_CCMFrame']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//div[@data-mz-key='clinical.vital.all'])</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class='clinical lab k-grid has-wizard list-mode-only mz-widget k-widget k-editable batch-editing editable empty']/div[1]/div[1]</value>
      <webElementGuid>7c119f3e-6e1e-47d0-9ba4-0769532e4592</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_Close Popup/Obj_CCMFrame</value>
      <webElementGuid>c93c934a-58b9-4920-91f6-d0242802a5be</webElementGuid>
   </webElementProperties>
</WebElementEntity>
