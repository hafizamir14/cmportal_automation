<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_VitalSign_Plus</name>
   <tag></tag>
   <elementGuidId>6203f88d-0c3c-472b-97d9-548a9e93f4ff</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//div[@data-icon-img='labs results.png']//button)[1][count(. | //*[@ref_element = 'Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_Close Popup/Obj_CCMFrame']) = count(//*[@ref_element = 'Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_Close Popup/Obj_CCMFrame'])]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//div[@data-icon-img='vital_signs.png'])/div[1]//div[3]//button[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//div[@data-icon-img='labs results.png']//button)[1]</value>
      <webElementGuid>faa1baaa-191c-4d88-9723-1458bbdc882c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_Close Popup/Obj_CCMFrame</value>
      <webElementGuid>35c3a1d8-e1cf-4227-b87a-9faf9127ecd1</webElementGuid>
   </webElementProperties>
</WebElementEntity>
