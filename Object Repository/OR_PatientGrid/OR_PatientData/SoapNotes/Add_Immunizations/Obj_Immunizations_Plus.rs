<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_Immunizations_Plus</name>
   <tag></tag>
   <elementGuidId>7dc637ff-6bd2-4cf2-9577-ce450256ce20</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//div[@data-icon-img='immunization.png'])/div[1]//div[2]//button[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>(//div[@data-icon-img='labs results.png']//button)[1][count(. | //*[@ref_element = 'Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_Close Popup/Obj_CCMFrame']) = count(//*[@ref_element = 'Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_Close Popup/Obj_CCMFrame'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//div[@data-icon-img='labs results.png']//button)[1]</value>
      <webElementGuid>52d57cd7-cec6-4e5d-9279-537384159d05</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_Close Popup/Obj_CCMFrame</value>
      <webElementGuid>f7147a57-ec57-4e45-ae4b-d95dd66f30b7</webElementGuid>
   </webElementProperties>
</WebElementEntity>
