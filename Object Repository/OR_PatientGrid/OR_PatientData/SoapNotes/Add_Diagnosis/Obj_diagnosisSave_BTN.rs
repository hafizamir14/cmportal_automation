<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_diagnosisSave_BTN</name>
   <tag></tag>
   <elementGuidId>f54c41f2-3371-44a2-a176-a107d6fce439</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button[id='encounterSoapBtnSaveNClose']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ref_element = 'Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_Close Popup/Obj_CCMFrame']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[@class='btn btn-primary btn-sm add-analyte']</value>
      <webElementGuid>54b58751-8dcf-458d-8527-3e59db6f4f3d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_Close Popup/Obj_CCMFrame</value>
      <webElementGuid>b2d4567c-29a1-4cdd-8ab2-0af16f1923c9</webElementGuid>
   </webElementProperties>
</WebElementEntity>
