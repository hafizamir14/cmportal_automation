<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_RenderingProvider_Select</name>
   <tag></tag>
   <elementGuidId>d2167e34-301d-4692-b80d-58fde81f6e5d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#carePlanPatientInfoDiv_550910 > div</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='soapRenderProvider-list']/div[3]/ul/li[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>86985da1-ffe1-4459-aed2-0884a7dbc5e4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Ahmad, Murtaza</value>
      <webElementGuid>ab5bb68b-3b29-4f4e-80ec-4c3428945740</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;carePlanPatientInfoDiv_550910&quot;)/div[1]</value>
      <webElementGuid>09d074b7-9870-4081-975e-692190c9e7e0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_Close Popup/Obj_CCMFrame</value>
      <webElementGuid>c81be02b-bf0c-422c-a326-149f559867e4</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='carePlanPatientInfoDiv_550910']/div</value>
      <webElementGuid>f5225e78-6eb1-49e8-b541-9848504688e8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Reject'])[1]/following::div[6]</value>
      <webElementGuid>e575e279-5076-4578-8a53-ebcb37c8a83e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Save &amp; Approve'])[1]/following::div[6]</value>
      <webElementGuid>cfc38a86-45f2-43a4-9129-b7f82e817a15</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='DOB'])[6]/preceding::div[2]</value>
      <webElementGuid>ce5c28af-ccbc-46d8-b7bb-dd03625ddfc3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[157]/div[2]/div[2]/div/div/div/div/div</value>
      <webElementGuid>349b722c-9409-4fd7-8935-e1c7dd25458a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
