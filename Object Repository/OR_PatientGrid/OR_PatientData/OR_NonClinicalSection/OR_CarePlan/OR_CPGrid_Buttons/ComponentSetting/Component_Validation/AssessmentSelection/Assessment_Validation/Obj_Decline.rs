<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_Decline</name>
   <tag></tag>
   <elementGuidId>e48655d4-317f-4076-a022-60b9ca08797f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//textarea[contains(@id, 'carePlanAssessmentNotes') and contains(text(), 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Most of the time' or . = 'Most of the time') and @ref_element = 'Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_Close Popup/Obj_CCMFrame']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>9ec9f264-fa89-49e7-8d72-9208b16000b4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>text-overflow-tooltip</value>
      <webElementGuid>6f37bbb5-a2d1-4a03-8ff1-efc2321b27be</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Most of the time</value>
      <webElementGuid>c301ffaa-c39f-4024-88d4-b4ee1fb0db9c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;grdCCAssessment_516296&quot;)/div[@class=&quot;k-header k-grid-toolbar&quot;]/div[@class=&quot;k-grid-content assessment-view-multiple&quot;]/div[@class=&quot;assessment-questions-answers-grid_7712 k-grid k-widget k-editable&quot;]/div[@class=&quot;k-grid-content k-auto-scrollable&quot;]/table[@class=&quot;k-selectable&quot;]/tbody[1]/tr[1]/td[2]/span[@class=&quot;text-overflow-tooltip&quot;]</value>
      <webElementGuid>7e50719b-0556-48fa-af9b-9b78c315a130</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_Close Popup/Obj_CCMFrame</value>
      <webElementGuid>b76e5781-739d-47ea-a9ff-da3cee2ef034</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='grdCCAssessment_516296']/div/div/div[3]/div[3]/table/tbody/tr/td[2]/span</value>
      <webElementGuid>7a0acb72-3cfb-48ef-b208-9614f5551a79</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Column Settings'])[176]/following::span[2]</value>
      <webElementGuid>9ae17170-a013-4ec9-8a13-a1d1e29d468a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Column Settings'])[175]/following::span[3]</value>
      <webElementGuid>4d6dd6b3-b773-481b-9b10-fafcf54f6bf6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Delete'])[11]/preceding::span[2]</value>
      <webElementGuid>0299b5d7-7d22-46f9-89c8-771a675921a6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Public transport'])[2]/preceding::span[4]</value>
      <webElementGuid>9a2f6aba-9467-411e-9bbe-77647f0e072e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[7]/div/div/div[3]/div[3]/table/tbody/tr/td[2]/span</value>
      <webElementGuid>3ea1b784-88c0-43e0-b8cb-0d3da1680151</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
