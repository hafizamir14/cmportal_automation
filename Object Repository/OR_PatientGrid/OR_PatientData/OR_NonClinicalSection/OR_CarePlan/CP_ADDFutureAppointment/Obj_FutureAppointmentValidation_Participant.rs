<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Obj_FutureAppointmentValidation_Participant</name>
   <tag></tag>
   <elementGuidId>09db8e77-3b2c-4e07-b4f6-dfc9cbb2d48a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//div[contains(@id,'ccmAppointmentsDiv')])//td[contains(text(), 'Amir, Hafiz')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/OR_Close Popup/Obj_CCMFrame</value>
      <webElementGuid>1c20b0d4-6e5e-4d0e-991b-ced7e042bcb8</webElementGuid>
   </webElementProperties>
</WebElementEntity>
