$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Include/features/PatientGrid/SMSFunctionality/SMSFunctionality.feature");
formatter.feature({
  "name": "Verify SMS notification is working fine",
  "description": "",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "name": "SMS sent from patient grid to PWB and verify sms on Communication Center",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@VerifySMSfromPatientGridAppointment"
    },
    {
      "name": "@Regression"
    },
    {
      "name": "@Sanity"
    }
  ]
});
formatter.step({
  "name": "I search \u003cPatient\u003e using global search",
  "keyword": "Given "
});
formatter.step({
  "name": "I click on appointment tab",
  "keyword": "* "
});
formatter.step({
  "name": "I click on edit button to edit appointment",
  "keyword": "* "
});
formatter.step({
  "name": "I set visit type as telehealth in PWB",
  "keyword": "* "
});
formatter.step({
  "name": "I verify that copy button and send sms button available and functional",
  "keyword": "* "
});
formatter.step({
  "name": "I set start date for appointment",
  "keyword": "* "
});
formatter.step({
  "name": "I set end date for appointment",
  "keyword": "* "
});
formatter.step({
  "name": "I click the Send SMS button from schedual appointment",
  "keyword": "* "
});
formatter.step({
  "name": "I should see the Invite Via Text Message window",
  "keyword": "* "
});
formatter.step({
  "name": "I select the \u003cLanguage\u003e in which the invitation is to be sent from language dropdown",
  "keyword": "* "
});
formatter.step({
  "name": "I enter \u003cPhone\u003e to send message",
  "keyword": "* "
});
formatter.step({
  "name": "I click the Send SMS button",
  "keyword": "* "
});
formatter.step({
  "name": "I should see the Message sent successfully notification",
  "keyword": "* "
});
formatter.step({
  "name": "I click the save button to save Appointments",
  "keyword": "When "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "Filter",
        "Patient",
        "Language",
        "Phone",
        "Message_Text"
      ]
    },
    {
      "cells": [
        "Contains",
        "Adam, Andrew",
        "Spanish",
        "+14245021130",
        "Edit message"
      ]
    }
  ]
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I navigate to patient grid",
  "keyword": "Given "
});
formatter.match({
  "location": "SD_PatientGrid.I_navigate_to_patient_grid()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "SMS sent from patient grid to PWB and verify sms on Communication Center",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@VerifySMSfromPatientGridAppointment"
    },
    {
      "name": "@Regression"
    },
    {
      "name": "@Sanity"
    }
  ]
});
formatter.step({
  "name": "I search Adam, Andrew using global search",
  "keyword": "Given "
});
formatter.match({
  "location": "SD_SearchPatient.search_Patient(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click on appointment tab",
  "keyword": "* "
});
formatter.match({
  "location": "SMSverification.appointmenttab()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click on edit button to edit appointment",
  "keyword": "* "
});
formatter.match({
  "location": "SMSverification.editappointment()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I set visit type as telehealth in PWB",
  "keyword": "* "
});
formatter.match({
  "location": "SMSverification.visittypecc()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I verify that copy button and send sms button available and functional",
  "keyword": "* "
});
formatter.match({
  "location": "SMSverification.veryfybuttons()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I set start date for appointment",
  "keyword": "* "
});
formatter.match({
  "location": "SMSverification.adate()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I set end date for appointment",
  "keyword": "* "
});
formatter.match({
  "location": "SMSverification.edate()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click the Send SMS button from schedual appointment",
  "keyword": "* "
});
formatter.match({
  "location": "SMSverification.sendsmsbtn()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should see the Invite Via Text Message window",
  "keyword": "* "
});
formatter.match({
  "location": "SMSverification.Invite_via_Text_Message()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I select the Spanish in which the invitation is to be sent from language dropdown",
  "keyword": "* "
});
formatter.match({
  "location": "SMSverification.languageselect(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I enter +14245021130 to send message",
  "keyword": "* "
});
formatter.match({
  "location": "SMSverification.sphonefield(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click the Send SMS button",
  "keyword": "* "
});
formatter.match({
  "location": "SMSverification.lsendsmsbuttonnextpopuppaget()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should see the Message sent successfully notification",
  "keyword": "* "
});
formatter.match({
  "location": "SMSverification.sendnotificationmessage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click the save button to save Appointments",
  "keyword": "When "
});
formatter.match({
  "location": "SMSverification.saveappointmentbutn()"
});
formatter.result({
  "status": "passed"
});
});