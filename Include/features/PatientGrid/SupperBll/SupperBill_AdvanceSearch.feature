Feature: Supper Bill Advance Search Filters

  Background: 
    Given I navigate to patient grid With Supper Bill

  @SmokeUSMM_SupperBill_SimpleSearch
  Scenario Outline: Verify that Search Filters Is Working
    When I click on Reset button to reset supper bill filters
    And I drag first slider
    * I click on slider search button
    Then I verify <SliderProvider> as slider filter is working fine
    * I select <SliderProvider> as slider filter provider
    * I click on Apply button to apply supper bill filters

    Examples: 
      | SliderProvider |
      | Dr, Brown      |

  @SmokeUSMM_SupperBill_AdvSearch_Range_LN
  Scenario Outline: Verify that Advance Search Filters Using Range & LastName are Working
    When I click on Reset button to reset supper bill filters
    And I click on advance search link
    Then I should see <AdvTitle> as title
    When I checked range checkbox
    And I drag adv search slider
    * I click on advance search button
    Then I verify that search button is disabled
    * I verify <SliderProvider> as slider filter is working fine
    * I select <SliderProvider> as slider filter provider
    * I click on Apply button to apply supper bill filters

    Examples: 
      | SliderProvider | AdvTitle       |
      | Dr, Brown      | Advance Search |

  @SmokeUSMM_SupperBill_AdvSearch_Range_FN
  Scenario Outline: Verify that Advance Search Filters Using Range & FirstName are Working
    When I click on Reset button to reset supper bill filters
    And I click on advance search link
    Then I should see <AdvTitle> as title
    When I checked firstname checkbox
    And I checked range checkbox
    * I drag adv search slider
    * I click on advance search button
    Then I verify that search button is disabled
    * I verify <SliderProvider> as slider filter is working fine
    * I select <SliderProvider> as slider filter provider
    * I click on Apply button to apply supper bill filters

    Examples: 
      | SliderProvider   | AdvTitle       |
      | Dr, Brown | Advance Search |

  @SmokeUSMM_SupperBill_AdvSearch_StarsWith_LN
  Scenario Outline: Verify that Advance Search Filters Using StartsWith & LastName are Working
    When I click on Reset button to reset supper bill filters
    And I click on advance search link
    Then I should see <AdvTitle> as title
    When I checked startsWith checkbox
    And I enter <Keywords> as startsWith filters
    * I click on advance search button
    Then I verify that search button is disabled
    * I verify <SliderProvider> as slider filter is working fine
    * I select <SliderProvider> as slider filter provider
    * I click on Apply button to apply supper bill filters

    Examples: 
      | SliderProvider   | AdvTitle       | Keywords |
      | User, Automation | Advance Search | us       |

  @SmokeUSMM_SupperBill_AdvSearch_StarsWith_FN
  Scenario Outline: Verify that Advance Search Filters Using StartsWith & FirstName are Working
    When I click on Reset button to reset supper bill filters
    And I click on advance search link
    Then I should see <AdvTitle> as title
    When I checked firstname checkbox
    And I checked startsWith checkbox
    * I enter <Keywords> as startsWith filters
    * I click on advance search button
    Then I verify that search button is disabled
    * I verify <SliderProvider> as slider filter is working fine
    * I select <SliderProvider> as slider filter provider
    * I click on Apply button to apply supper bill filters

    Examples: 
      | SliderProvider   | AdvTitle       | Keywords |
      | User, Automation | Advance Search | au       |
