Feature: Soap Note Creation from Schedule flow

  Background: 
    Given I navigate to CMR_Schedule


  
@Smoke_USMM_CreateBillingInformation

  Scenario Outline: Verify Creating Billing Information  - Based On Patient's Medical Record
    Then I should see already scheduled appointment
    When I click on three dots
    * I click on edit soapnotes
    Then I should see <Patient> as patient
    When I click on Billing Information Edit button
    And I click on Add button to billing info
    * I enter <BillingInformation> as billing information procedure
    * I click on billing information OK button
    * I click on SaveClose button to save billing information
    Then I should see Billing Information data in soap note popup
    When I click on Save button to save SOAP NOTE
    And I should see soap note saved message
    * I click on SaveClose button to save SOAP NOTE
    * I click on three dots
    * I should see Edit Soap Note option
    Then I should see Billing Information data in soap note popup

    Examples: 
      | BillingInformation |Patient|
      |              99203 |Dermo505, Mac505|

 
