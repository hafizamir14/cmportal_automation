Feature: Soap Note Creation from Schedule flow

  Background: 
    Given I navigate to CMR_Schedule

  @Smoke_USMM_CreateLabResults
  Scenario Outline: Verify Creating Lab Results
    Then I should see already scheduled appointment
    When I click on three dots
    * I click on edit soapnotes
    * I click on add Lab Results plus button
    And I enter <LabResults> as lab results in search and select analyte
    * I click on lab resutls OK button
    Then I should see Lab Resultss data in soap note popup
    When I click on Save button to save SOAP NOTE
    Then I should see soap note saved message
    When I click on SaveAll button to save Lab Results
    * I click on SaveClose button to save SOAP NOTE
    And I should see soap note saved message
    * I click on three dots
    * I should see Edit Soap Note option
    * I should see Lab Resultss data in soap note popup

    Examples: 
      | LabResults | Patient          |
      | A1C        | Dermo505, Mac505 |
