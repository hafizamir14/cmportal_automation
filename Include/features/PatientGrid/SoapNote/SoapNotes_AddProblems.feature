Feature: Soap Note Creation from Schedule flow

  Background: 
    Given I navigate to CMR_Schedule


 @Smoke_USMM_CreatingProblems
	Scenario Outline: Verify Creating Problems
		Then I should see already scheduled appointment
		When I click on three dots
		   * I click on edit soapnotes
		Then I should see <Patient> as patient
		When I click on add Problems plus button
		 And I enter <ProblemsCode> as the problems code
		   * I enter <Problems_Start_Date> as problems start date
		   * I enter <Comment> as problemcomment
		   * I click on saveclose button to save problems
		Then I should see problems data in soap note popup
		When I click on Save button to save SOAP NOTE
		Then I should see soap note saved message
		When I click on SaveClose button to save SOAP NOTE
		 And I click on three dots
		   * I should see Edit Soap Note option
		Then I should see problems data in soap note popup

		Examples: 
			| ProblemsCode      | Problems_Start_Date | Patient          | Comment         |
			| diabetes mellitus |            04162021 | Dermo505, Mac505 | I'm testing to add problems |
