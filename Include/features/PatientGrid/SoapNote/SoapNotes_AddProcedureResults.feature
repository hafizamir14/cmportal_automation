Feature: Soap Note - Add Procedure Results

	Background: 
		Given I navigate to CMR_Schedule

	@Smoke_USMM_CreateProcedure
	Scenario Outline: Verify Creating Procedure Results
		When I click on three dots
		 And I click on edit soapnotes
		Then I should see <Patient> as patient
		When I click on add Procedure Results plus button
		 And I enter <Procedure> as procedure
		   * I enter <Procedure_Date> as Procedure_Date
		   * I enter <ProcedureTargetSite> as ProcedureTargetSite
#		   * I enter <Procedure_Reason> as Procedure_Reason
		   * I enter <Procedure_Location> as Procedure_Location
		   * I enter <Procedure_Note> as Procedure_Note
		   * I click on ProcedureSaveClose button
		Then I should see <Procedure> and <Procedure_Date> and <ProcedureTargetSite> and <Procedure_Reason> and <Procedure_Location> and <Procedure_Note> as ProcedureResults data in soap note grid

		Examples: 
			| Procedure                        | Procedure_Date | ProcedureTargetSite              | Procedure_Reason                   | Patient          | Procedure_Location | Procedure_Note    |
			| Anesth, open head surgery::00210 |       04162020 | Abnormal cell (cell) :: 39266006 | Accidental fall (event)::217082002 | Dermo505, Mac505 | Test               | Procedure Comment |
