Feature: Care Plan - ADD Future Appointment

	Background: 
		Given I navigate to patient grid

	@Smoke_USMM
	Scenario Outline: Verify That User Is Able to Add Future Appointment
		When I search <Patient> using global search
		 And I click on care plan tab
		   * I click on add new care plan button
		   * I click on basedonpatientmedicalrecord
		   * I hover over on future appointment
		   * I click on future appointment plus button
		   * I enter <FutureAppointmentReason> as future appointment reason
		   * I enter <FA_Participant> as future appointment participant
		   * I enter <FP_Date> as future appointment date
		   * I enter <FA_StartTime> as future appointment start time
		   * I enter <FA_EndTime> as future appointment end time
		   * I click on updated button
		   * I enter title <Title>
		   * I click on save and close button
		   * I click on title from care plan grid
		Then I should see patient <Patient> as patient_name
		Then I should see <FutureAppointmentReason> and <FA_Participant> and <FP_Date> as updated future appointment
		
		Examples: 
			| Patient          | FutureAppointmentReason | SucessMessage                           | FP_Date  | Title                 | FA_Participant | FA_StartTime | FA_EndTime |
			| Dermo505, Mac505 | Confusion               | successCare Plan Saved SuccessfullyHide | 12062022 | Future Appointment | Amir, Hafiz    | 8:30 AM      | 9:00 AM    |
