package patientGrid
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.Keys

import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import cucumber.api.java.en.And
import utility_Functions.UtilityFunctions





class SD_PatientTimerActivityChoices {
	TestObject CMF=findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/CareManagementForm/obj_caremanagement')
	TestObject NonBillableClick=findTestObject('Object Repository/OR_PatientGrid/PatientTimer/TimerActivityChoices/Obj_ProgramDropdownClick')
	TestObject frame=findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/CareManagementForm/Obj_CCMFrame')
	UtilityFunctions obj=new UtilityFunctions();


	@And("I select Program Dropdown as:(.*)")
	public void i_select_ProgramDropdownValue(String Program) {


		'I click on Non Billable field'
		WebUI.click(NonBillableClick)
		'I select value from the dropdown'
		String xpath='//ul[@id="serviceType_listbox"]/li[contains(text(), "'+Program+'")]'
		obj.selectdropdown(frame,xpath)

		WebUI.click(findTestObject("Object Repository/OR_PatientGrid/PatientTimer/TimerActivityChoices/Obj_Popup_ClickNO"))

		Thread.sleep(2000)
	}

	@And("I select (.*) as CPT Code")
	public void SelectCPTCode(String CPTCode) {

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/PatientTimer/TimerActivityChoices/Obj_ClickCPTCode'))
		Thread.sleep(1000)

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/PatientTimer/TimerActivityChoices/Obj_EnterCPTCode'), CPTCode)

		Thread.sleep(2000)

		WebUI.sendKeys(findTestObject('Object Repository/OR_PatientGrid/PatientTimer/TimerActivityChoices/Obj_EnterCPTCode'), Keys.chord(Keys.ENTER))
	}

	@And("I enter (.*) as time duration")
	public void EnterTime(String Time) {


		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/PatientTimer/TimerActivityChoices/Obj_DurationInput'), Time)
	}

	@And("I select (.*) as Completed Actions")
	public void SelectCompletedAction(String CompletedAction) {

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/PatientTimer/TimerActivityChoices/Obj_CompletedAction_Click'))
		Thread.sleep(1000)

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/PatientTimer/TimerActivityChoices/Obj_CompletedAction_Input'), CompletedAction)
		Thread.sleep(1000)
		WebUI.sendKeys(findTestObject('Object Repository/OR_PatientGrid/PatientTimer/TimerActivityChoices/Obj_CompletedAction_Input'), Keys.chord(Keys.ENTER))
	}

	@And("I select (.*) as Completed Actions2")
	public void SelectCompletedAction2(String CompletedAction2) {

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/PatientTimer/TimerActivityChoices/Obj_CompletedAction_Click'))
		Thread.sleep(1000)

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/PatientTimer/TimerActivityChoices/Obj_CompletedAction_Input'), CompletedAction2)
		Thread.sleep(1000)
		WebUI.sendKeys(findTestObject('Object Repository/OR_PatientGrid/PatientTimer/TimerActivityChoices/Obj_CompletedAction_Input'), Keys.chord(Keys.ENTER))

		Thread.sleep(2000)
	}

	@And("I select (.*) as Completed Actions3")
	public void SelectCompletedAction3(String CompletedAction3) {

		WebUI.scrollToPosition(30, 30)
		WebUI.scrollToElement(findTestObject("Object Repository/OR_PatientGrid/PatientTimer/Obj_PatientTimer_Comment"), 3)

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/PatientTimer/TimerActivityChoices/Obj_CompletedAction_Click'))
		Thread.sleep(1000)

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/PatientTimer/TimerActivityChoices/Obj_CompletedAction_Input'), CompletedAction3)
		Thread.sleep(1000)
		WebUI.sendKeys(findTestObject('Object Repository/OR_PatientGrid/PatientTimer/TimerActivityChoices/Obj_CompletedAction_Input'), Keys.chord(Keys.ENTER))
		Thread.sleep(1000)
	}


	@And("I select (.*) as diagnosis")
	public void SelectDiagnosis(String Diagnosis) {

		WebUI.click(findTestObject('Object Repository/OR_PatientGrid/PatientTimer/TimerActivityChoices/Obj_ClickDiagnosis'))

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/PatientTimer/TimerActivityChoices/Obj_ClickDiagnosis_Input'), Diagnosis)

		Thread.sleep(2000)

		WebUI.sendKeys(findTestObject('Object Repository/OR_PatientGrid/PatientTimer/TimerActivityChoices/Obj_ClickDiagnosis_Input'), Keys.chord(Keys.ENTER))
	}

	@And("I should see patient timer (.*) and (.*) and (.*) historyData")
	public void PatientTimerHistoryData(String Program, String CPTCode, String Diagnosis) {

		String actualProgram = WebUI.getText(findTestObject("Object Repository/OR_PatientGrid/PatientTimer/TimerActivityChoices/Obj_PatientTimerHistory_ProgramValidation"))
		WebUI.verifyMatch(actualProgram, Program, false)

		String actualDiagnosis = WebUI.getText(findTestObject("Object Repository/OR_PatientGrid/PatientTimer/TimerActivityChoices/Obj_PatientTimerHistory_DiagnosisValidation"))
		WebUI.verifyMatch(actualDiagnosis, Diagnosis, false)

		String actualCPTCode = WebUI.getText(findTestObject("Object Repository/OR_PatientGrid/PatientTimer/TimerActivityChoices/Obj_PatientTimerHistory_CPTValidation"))
		WebUI.verifyMatch(actualCPTCode, CPTCode, false)

		WebUI.verifyElementPresent(findTestObject('Object Repository/OR_PatientGrid/PatientTimer/Obj_PatientTimer_TimeSpent'), 3)
	}

	@And("I should see patient timer (.*) as historyData")
	public void PatientTimerHistoryDataProgram(String Program) {

		String actualProgram = WebUI.getText(findTestObject("Object Repository/OR_PatientGrid/PatientTimer/TimerActivityChoices/Obj_PatientTimerHistory_ProgramValidation"))
		WebUI.verifyMatch(actualProgram, Program, false)

		WebUI.verifyElementPresent(findTestObject('Object Repository/OR_PatientGrid/PatientTimer/Obj_PatientTimer_TimeSpent'), 3)
	}
}