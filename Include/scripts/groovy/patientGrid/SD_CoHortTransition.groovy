package patientGrid
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import cucumber.api.java.en.And
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class SD_CoHortTransition {


	@And("I click on save cohort from dropdown")
	public void CoHortSaveDropdown() {


		WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/CoHort/Obj_CoHortBTNDropdownClick'))
		Thread.sleep(1000)
		WebUI.click(findTestObject('Object Repository/CareCordination_LeftFilters/CoHort/Obj_CoHortBTNSave'))
		Thread.sleep(1000)
	}

	@Then("I should see (.*) as save cohort as")
	public void VerifyCoHortTitle(String Title) {

		String actualTitle = WebUI.getText(findTestObject("Object Repository/CareCordination_LeftFilters/CoHort/Obj_CoHortSaveTitle"))
		WebUI.verifyMatch(actualTitle, Title, false)
	}

	@When("I enter (.*) as cohort name")
	public void EnterCoHortName(String CoHortName) {

		Thread.sleep(2000)
		WebUI.setText(findTestObject("Object Repository/CareCordination_LeftFilters/CoHort/Obj_CoHort_SaveInput"), CoHortName)
	}

	@And("I click on cohort save button")
	public void CoHortSaveBTNs() {

		Thread.sleep(2000)
		WebUI.click(findTestObject("Object Repository/CareCordination_LeftFilters/CoHort/Obj_CoHortSave"))
		Thread.sleep(1000)
	}

	@Then("I should see (.*) as cohort message")
	public void VerifyCoHortMessage(String Message) {

		String actualMessage = WebUI.getText(findTestObject("Object Repository/CareCordination_LeftFilters/CoHort/Obj_CoHortSaveMessage"))
		WebUI.verifyMatch(actualMessage, Message, false)
	}

	@And("I click on cross button to delete cohort from dropdown")
	public void CoHortDeleteBTNDropdown() {

		Thread.sleep(2000)
		WebUI.click(findTestObject("Object Repository/CareCordination_LeftFilters/CoHort/Obj_CoHortDropdownClick"))
		Thread.sleep(1000)
		WebUI.click(findTestObject("Object Repository/CareCordination_LeftFilters/CoHort/Obj_CoHortDeleteClick"))
	}
	@And("I click on proceed button to confirm delete cohort")
	public void CoHortProceedBtn() {

		Thread.sleep(2000)
		WebUI.click(findTestObject("Object Repository/CareCordination_LeftFilters/CoHort/Obj_CoHortProceedBTN"))
		Thread.sleep(3000)
	}

	@And("I select added cohort from dropdown")
	public void CoHortSelect() {

		Thread.sleep(2000)
		WebUI.click(findTestObject("Object Repository/CareCordination_LeftFilters/CoHort/Obj_CoHortDropdownSelect"))
		Thread.sleep(3000)
	}
}