package patientGrid
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.Keys

import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import cucumber.api.java.en.And
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import utility_Functions.UtilityFunctions



public class SD_DFT_ProgramEnrollmentPlusTasks {

	UtilityFunctions obj=new UtilityFunctions();

	TestObject titleverify=findTestObject('Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_EligiblePopupTitleVerification')
	TestObject frame=findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_NonClinicalSection/OR_CarePlan/Notes/Forms/CareManagementForm/Obj_CCMFrame')
	TestObject ReasonDropdown=findTestObject('Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_ReasonDropdownClick')
	TestObject ConditionDropdown=findTestObject('Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_ConditionFieldClick')
	TestObject ProblemDropdown=findTestObject('Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_ProblemFieldDropdownClick')
	TestObject ReferralDropdown=findTestObject('Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_ReferralSourceClick')
	TestObject StatusDropdown=findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_CareGap/OR_Task/Obj_statusdropdown')
	TestObject OwnerDropdown=findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_CareGap/Obj_Ownerdropdown')




	@And("I click on program enrollment dropdown")
	void I_Click_on_programEnrollment() {
		WebUI.click(findTestObject("Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_ProgramEnrollmentClick"))

		Thread.sleep(2000)
	}

	@And("I expand BHCM")
	void I_Click_on_BHCM() {
		WebUI.click(findTestObject("Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_BHCMClick"))
		Thread.sleep(1000)
	}

	@And("I click on eligible link")
	void I_Click_on_EligibleLink() {
		WebUI.click(findTestObject("Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_EligilbleClick"))
		Thread.sleep(2000)
	}
	@And("I click on eligible button")
	void I_Click_on_EligibleBTN() {
		WebUI.click(findTestObject("Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_EligibleBTN"))
		Thread.sleep(2000)
	}

	@And("I should see popup with correct title")
	public void I_should_see_CorrectTitle() {

		String actual_Title = WebUI.getText(titleverify)

		WebUI.verifyEqual(actual_Title,'Reason')
	}

	@And("I select reason:(.*)")
	public void i_select_reason(String Reason) {

		WebUI.click(ReasonDropdown)
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//li[text()="'+Reason+'"]'
		obj.selectdropdown(frame,xpath)
		Thread.sleep(2000)
	}

	@And("I select problems:(.*)")
	public void i_select_problems(String Problems) {
		WebUI.click(ProblemDropdown)

		Thread.sleep(2000);

		WebUI.clearText(findTestObject("Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_ProblemFieldInput"))
		Thread.sleep(1000);

		WebUI.setText(findTestObject("Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_ProblemFieldInput"), Problems)
		Thread.sleep(1000);

		WebUI.sendKeys(findTestObject('Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_ProblemFieldInput'), Keys.chord(Keys.ENTER))

		Thread.sleep(2000)
	}

	@And("I select referral source:(.*)")
	public void i_select_ReferralSource(String ReferralSource) {
		WebUI.click(ReferralDropdown)
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='//li[text()="'+ReferralSource+'"]'
		obj.selectdropdown(frame,xpath)
		Thread.sleep(2000)
	}

	@And("I enter (.*) as conditions")
	public void EnterCondition(String Conditions) {

		WebUI.click(ConditionDropdown)

		WebUI.sendKeys(findTestObject('Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_ConditionFieldInput'), Keys.chord(Keys.BACK_SPACE))

		Thread.sleep(1000)

		WebUI.setText(findTestObject("Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_ConditionFieldInput"), Conditions)
		Thread.sleep(1000)
		WebUI.sendKeys(findTestObject('Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_ConditionFieldInput'), Keys.chord(Keys.ENTER))
		Thread.sleep(2000)
	}

	@And("I enter (.*) as customs condition")
	public void EnterCustomsCondition(String Customs_Conditions) {

		WebUI.setText(findTestObject("Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_CustomConditionInput"), Customs_Conditions)
		Thread.sleep(2000)
	}

	@Then("I should see the latest record (.*),(.*),(.*),(.*) after expanding the enrollment history")
	void EnrollmentHistoryverification(String UpdatedBy,String status,String Reason,String Problems) {


		//		String UpdatedByxpath='(//*[@class="k-grid-content k-auto-scrollable"]//*[text()="'+UpdatedBy+'"])[1]'
		//		String optionxpath='(//*[@class="k-grid-content k-auto-scrollable"]//*[text()="'+status+'"])[1]'
		//		String reasonxpath='(//*[@class="k-grid-content k-auto-scrollable"]//*[text()="'+Reason+'"])[1]'
		//		String Problemsxpath='(//*[@class="k-grid-content k-auto-scrollable"]//*[text()="'+Problems+'"])[1]'


		//		WebUI.verifyEqual(UpdatedBy, actualUpdated)
		//		WebUI.verifyEqual(status, actualOption)
		//		WebUI.verifyEqual(Reason, actualReason)
		//		WebUI.verifyEqual(Problems, actualProblems)

		String actualStatus = WebUI.getText(findTestObject("Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_ValidationStatus"))
		WebUI.verifyEqual(actualStatus, status)

		String actualUpdatedBy = WebUI.getText(findTestObject("Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_ValidationUpdatedBy"))
		WebUI.verifyEqual(actualUpdatedBy, UpdatedBy)

		String actualReason = WebUI.getText(findTestObject("Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_ValidationReason"))
		WebUI.verifyEqual(actualReason, Reason)

		String actualProblem = WebUI.getText(findTestObject("Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_ValidationProblem"))
		WebUI.verifyEqual(actualProblem, Problems)

	}

	@Then("I should see the sequence of problem (.*) is sorted")
	void EnrollmentHistoryProblemverification(String Problems) {

		String actualProblem = WebUI.getText(findTestObject("Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_ValidationProblem"))
		WebUI.verifyEqual(actualProblem, Problems)
	}



	@When("I click on Task tab")
	void ClickOnTaskTab() {

		WebUI.click(findTestObject("Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_TaskBottomTabClick"))
	}

	@And("I click on Task main tab")
	void ClickOnTaskMainTab() {

		WebUI.click(findTestObject("Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_TaskTabClick"))
	}

	@And("I select the patient that is associated with task")
	void ClickOnPatientNameAssociated() {

		WebUI.click(findTestObject("Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_PatientNameClick"))
	}

	@And("I click on create task button")
	void ClickOnCreateTaskBTN() {

		WebUI.click(findTestObject("Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_AddTaskBTN"))
		Thread.sleep(1000)
		WebUI.click(findTestObject("Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_CustomTaskClick"))
		Thread.sleep(2000)
	}

	@Then("I should see Create New Task form with title")
	void VerifyCreateTaskTitleForm() {

		String actualTitle = WebUI.getText(findTestObject("Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_TaskCreationTitleVerification"))
		WebUI.verifyEqual(actualTitle, "Create New Task")
	}

	@And("I click on save button to save task")
	void TaskSaveBTN() {

		WebUI.click(findTestObject("Object Repository/OR_PatientGrid/DFT_Client_Cases/ProgramEnrollmentPlusTask/Obj_TaskSaveBTN"))
	}


	@And("I enter (.*) as task field")
	void buttoncreatetask(String Taskname) {

		'Click on Add button'
		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_CareGap/OR_Task/Obj_taskfield'), Taskname)
	}


	@And("I enter (.*) as task start date field")
	void Strtdatetask(String Strtdate) {

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_CareGap/OR_Task/Obj_startdate'), Strtdate)
	}


	@And("I enter (.*) as task completed on date field")
	void Completeddatetask(String Completeddate) {

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_CareGap/OR_Task/Obj_datecompletedon'), Completeddate)
	}


	@And("I enter (.*) as task status field")
	void statustask(String status) {
		Thread.sleep(2000)
		WebUI.click(StatusDropdown)
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='(//li[text()="'+status+'"])[2]'
		obj.selectdropdown(frame,xpath)
		Thread.sleep(2000)
	}


	@When("I enter (.*) as owner field")
	void ownertask(String owner) {


		Thread.sleep(2000)
		WebUI.click(OwnerDropdown)
		'I select value from the dropdown'
		Thread.sleep(2000);
		String xpath='(//li[text()="'+owner+'"])[2]'
		obj.selectdropdown(frame,xpath)
		Thread.sleep(2000)
	}

	@When("I enter (.*) as task description field")
	def Desriptionetask(String Desription) {

		WebUI.setText(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_CareGap/OR_Task/Obj_description'), Desription)
		WebUI.sendKeys(findTestObject('Object Repository/OR_PatientGrid/OR_PatientData/OR_CareGap/OR_Task/Obj_description'),Keys.chord(Keys.TAB))
	}


	@When("I should see added task as (.*) with status (.*) and start date (.*) end date (.*) and owner (.*) present in task section")
	def validationtask(String task, String status, String Strtdate, String Enddate, String Owner) {


		//		'Verify date of services'
		//		String date4 = DateTime.substring(0, 8)
		//
		//		String date5 = DateTime.substring(0, 8)
		//
		//		WebUI.verifyMatch(date4, date5, false)


		String smonth = Strtdate.substring(0, 2);
		String   sday = Strtdate.substring(2, 4);
		String  syear = Strtdate.substring(4, 8);

		String sdate1= ''+smonth+'/'+sday+'/'+syear+''

		System.out.println(sdate1)

		WebUI.verifyMatch(sdate1, Strtdate, false)

		String emonth = Enddate.substring(0, 2);
		String   eday = Enddate.substring(2, 4);
		String  eyear = Enddate.substring(4, 8);

		String edate1= ''+emonth+'/'+eday+'/'+eyear+''

		WebUI.verifyMatch(edate1, Strtdate, false)

		String TaskXpath='//span[text()="'+task+'"]'
		String OwnerXpath='//td[text()="'+Owner+'"]'
		String StatusXpath='//td[text()="'+status+'"]'


		'Verify Task'
		obj.customVerify(frame, task, TaskXpath);
		'Verify Owner'
		obj.customVerify(frame, Owner, OwnerXpath);
		'Verify Status'
		obj.customVerify(frame, status, StatusXpath);


	}
}